__author__ = 'random -at- sawrc.com'
import sys
sys.path.insert(0, 'lib')

import os
from collections import OrderedDict
from urllib import urlencode
import logging
import jinja2
import webapp2
from google.appengine.api import memcache, urlfetch
from google.appengine.api.urlfetch_errors import DownloadError
import simplejson as json
from bs4 import BeautifulSoup


SLPOST_URL = 'https://ipslight.upu.org/IPSLight.dll'
JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)


class EventField:
    evt_type, evt_local_dt, user, evt_office, next_office, condition, retention_reason = range(7)

    _desc = {
        0: "Event Type",
        1: "Event Timestamp",
        2: "User",
        3: "Event Office",
        4: "Next Office",
        5: "Item Condition",
        6: "Retention Reason"
    }

class TrackingException(Exception):
    pass

def get_tracking_details(tracking_number, retries=4):
    """

    :param tracking_number: Tracking number.
    :param retries: Number of retries to login.
    :return: tuple(more_details, events)
    :raises ValueError: Package not found.
    :raises TrackingException: Could not login to server.
    """
    tdetails = memcache.get('tracking{}'.format(tracking_number))
    if tdetails:
        logging.info("Previous cached entry found. Serving from cache.")
        return tdetails
    logging.info("Tracking package, {}".format(tracking_number))
    for attempt in range(retries):
        if attempt < 1:
            hdn_uid = memcache.get('hdn_uid')
            if hdn_uid is None:
                logging.debug("hdn_uid not in memcache.")
                continue
            else:
                logging.debug("Got hdn_uid from memcache.")
        else:
            try:
                home_page = urlfetch.fetch(
                    SLPOST_URL,
                    urlencode({'txt_userid': 'slpost', 'txt_password': 'slpost'}),
                    urlfetch.POST, validate_certificate=True)
            except DownloadError, e:
                logging.error(e.message)
                continue
            main_soup = BeautifulSoup(home_page.content, 'lxml')
            if len(main_soup.select('input#txt_password')) > 0:
                logging.error("New login request expired at login.")
                # Retrieval failed
                continue
            hdn_uid = main_soup.select('#hdn_uid')[0]['value']
            logging.debug("Logged in and set hdn_uid='{}' in memcache.".format(hdn_uid))
            memcache.set('hdn_uid', hdn_uid)
        try:
            tracking_page = urlfetch.fetch(
                '{url}?{param}'.format(
                    url=SLPOST_URL, param=urlencode({'uid': hdn_uid, 'fid': '205'})),
                urlencode({'hdn_uid': hdn_uid, 'id_txt': tracking_number}),
                urlfetch.POST, validate_certificate=True)
        except DownloadError, e:
            logging.error(e.message)
            continue
        tracking_soup = BeautifulSoup(tracking_page.content, 'lxml')
        if len(tracking_soup.select('input#txt_password')) > 0:
            # Retrieval failed, session expired
            logging.debug("Session expired.")
            continue
        elif any('not found' in cls.text for cls in tracking_soup.select('.cls_msg1')):
            raise ValueError("Package not Found.")
        details_table = tracking_soup.select('form > table > tr > td > table')[0]
        more_detail_table, event_table = details_table.select('> tr > th > table')

        events = []
        event_rows = event_table.select('#item_Events tbody tr')
        for row in event_rows:
            row_det = row.find_all('td')
            event = {}
            event[EventField.evt_type] = row_det[0].text
            event[EventField.evt_local_dt] = row_det[1].text
            event[EventField.user] = row_det[2].text
            event[EventField.evt_office] = row_det[3].text
            event[EventField.next_office] = row_det[4].text
            event[EventField.condition] = row_det[5].text
            event[EventField.retention_reason] = row_det[6].text
            events.append(event)

        more_details = OrderedDict()
        detail_rows = more_detail_table.find('table', class_='cls_item_list').find_all('tr')
        for row in detail_rows:
            try:
                if row.td['class'][0] == 'cls_item_dark':
                    field, value = row.find_all('td')
                    more_details[field.text] = value.text
            except (KeyError, TypeError):
                continue
        break
    else:
        raise TrackingException("Session expired after {} {}.".format(
            retries, 'retries' if retries > 1 else 'retry'))
    memcache.set('tracking{}'.format(tracking_number), (more_details, events), time=300)
    return more_details, events

class MainHandler(webapp2.RequestHandler):
    def get(self):
        logging.debug("Got a get request, {}".format(self.request.path_qs))
        if self.request.get('tid'):
            tracking_id = self.request.get('tid')
            try:
                det, ev = get_tracking_details(tracking_id)
            except ValueError:
                logging.info("Package '{}' not found".format(tracking_id))
                self.response.write('<p>Tracking ID ' + tracking_id + ' is invalid.</p>')
                self.response.write(
                    '''<input action="action" type="button" value="Back"
                        onclick="history.go(-1);" />''')
                return
            except TrackingException as e:
                logging.warning("Failed to retrieve details for package '{}\n{}'.".format(
                    tracking_id, e.message))
                self.response.write('<p>Could not track details for package ' + tracking_id + '.</p>')
                self.response.write(
                    '''<input action="action" type="button" value="Back"
                        onclick="history.go(-1);" />''')
                return
            logging.debug("Got package '{}' details.".format(tracking_id))
            template_values = {
                'more_details': det,
                'events': ev,
                'event_header': tuple(name for col, name in EventField._desc.items())
            }
            template = JINJA_ENVIRONMENT.get_template('index.html')
            web_page = template.render(template_values)
            self.response.write(web_page)
        else:
            template_values = {'more_details': None}
            template = JINJA_ENVIRONMENT.get_template('index.html')
            web_page = template.render(template_values)
            self.response.write(web_page)

class ApiHandler(webapp2.RequestHandler):
    def get(self):
        logging.debug("Got a api get request, {}".format(self.request.path_qs))
        if self.request.get('tid'):
            tracking_id = self.request.get('tid')
            try:
                det, ev = get_tracking_details(tracking_id)
            except ValueError:
                logging.debug("Invalid tracking id, '{}'".format(tracking_id))
                self.response.write(
                    json.dumps({'Error': 'Tracking id {} is invalid.'.format(tracking_id)}))
            except TrackingException as e:
                logging.warning("Failed to retrieve details for package '{}\n{}'.".format(
                    tracking_id, e.message))
                self.response.write(
                    json.dumps({'Error': 'Could not retrieve details for package {} .'.format(
                        tracking_id)}))
            else:
                logging.debug("Got package '{}' details.".format(tracking_id))
                json_str = json.dumps(
                    {
                        'Details': det,
                        'Events': [{EventField._desc[key]: evt[key] for key in evt} for evt in ev]
                    }
                )
                self.response.write(json_str)
        else:
            logging.debug("Tracking id not provided.")
            self.response.write(json.dumps({'error': "No tracking id 'tid' provided."}))

    def post(self):
        self.get()

app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/api', ApiHandler)
], debug=True)
